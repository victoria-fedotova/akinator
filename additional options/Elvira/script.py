import eel
import openai

openai.api_key = "sk-mPAvfoAY1qEwmPspDtfzT3B1bkFLNZ4TcX20DMuOjRTfkH"

eel.init('web')                     # Give folder containing web files

@eel.expose                         # Expose this function to Javascript
def handleinput(x):
    line = '% s' % x
    messages=[]
    messages.append({'role': 'system',
                     'content': line})
    try:
        completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=0.4)
        ask = completion.choices[0]['message']['content']
        messages.append({'role': 'assistant',
                         'content': ask})
        print(ask + str(completion.usage['total_tokens']))
        messages.append({'role': 'assistant',
                         'content': ask})
        eel.answer(line)

    except:
        #eel.answer('Извините, похоже закончились токены, Акинатор перезапустится')
        eel.answer(line)



eel.start('main.html', size=(500, 1000))    # Start
